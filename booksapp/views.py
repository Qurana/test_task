import datetime

import requests
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from booksapp.forms import BookForm
from booksapp.models import Book, Author


class BookListView(ListView):
    model = Book
    template_name = 'list_books.html'

    def get_queryset(self):
        # Get the search parameters from the request.
        search_title = self.request.GET.get('search_title', '')
        search_author = self.request.GET.get('search_author', '')
        search_language = self.request.GET.get('search_language', '')
        search_publication_date_from = self.request.GET.get('search_publication_date_from', '')
        search_publication_date_to = self.request.GET.get('search_publication_date_to', '')

        # Filter the books based on the search parameters.
        books = Book.objects.all()
        if search_title:
            books = books.filter(title__contains=search_title)
        if search_author:
            author = Author.objects.filter(author_name__contains=search_author)
            if not author:
                # Need to implement!
                return books
            books = books.filter(author=author[0])
        if search_language:
            books = books.filter(publication_language__contains=search_language)
        if search_publication_date_from and search_publication_date_to:
            books = books.filter(publication_date__range=(search_publication_date_from, search_publication_date_to))
        elif search_publication_date_from:
            books = books.filter(publication_date__gte=search_publication_date_from)
        elif search_publication_date_to:
            books = books.filter(publication_date__lte=search_publication_date_to)

        return books


class BookCreateView(CreateView):
    form_class = BookForm
    # model = Book
    # fields = '__all__'
    template_name = 'create_book.html'

    def get_success_url(self):
        return reverse_lazy('book_list')


class BookUpdateView(UpdateView):
    form_class = BookForm
    # model = Book
    # fields = '__all__'
    template_name = 'update_book.html'

    def get_success_url(self):
        return reverse_lazy('book_list')


class BookDeleteView(DeleteView):
    model = Book
    success_url = reverse_lazy('book_list')
    template_name = 'delete_book.html'

    def get_success_url(self):
        return reverse_lazy('book_list')


def book_import(request):
    if request.method == 'POST':
        # Get the book title from the request.
        book_title = request.POST['book_title']

        # Make a request to the Google Books API.
        response = requests.get(
            'https://www.googleapis.com/books/v1/volumes',
            params={'q': book_title}
        )

        # Check if the request was successful.
        if response.status_code == 200:
            # Get the book data from the response.
            book_data = response.json()['items'][0]['volumeInfo']

            # Create a list of authors from the book data.
            authors = []
            for author_name in book_data['authors']:
                author, created = Author.objects.get_or_create(author_name=author_name)
                authors.append(author)

            try:
                book_data['publishedDate'] = datetime.datetime.strptime(book_data['publishedDate'], "%Y-%m-%d").strftime("%Y")
            except ValueError:
                pass

            # Create a new book object in the database.
            book = Book(
                title=book_data['title'],
                publication_date=book_data['publishedDate'],
                isbn=book_data['industryIdentifiers'][0]['identifier'],
                number_of_pages=book_data['pageCount'],
                cover_link=book_data['imageLinks']['thumbnail'],
                publication_language=book_data['language'],
            )
            book.save()
            book.author.set(authors)

            # Return a redirect to the book list page.
            return HttpResponseRedirect(reverse_lazy('book_list'))
        else:
            # Display an error message to the user.
            return render(request, 'import_book.html', {'error_message': 'Could not import book.'})

    return render(request, 'import_book.html')