from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Author(models.Model):
    author_name = models.CharField(max_length=255)

    def __str__(self):
        return self.author_name


class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.ManyToManyField(Author)
    publication_date = models.PositiveIntegerField(verbose_name="Publication date",
                                                   validators=[
                                                       MaxValueValidator(9999),
                                                       MinValueValidator(1)
                                                   ])
    isbn = models.CharField(max_length=13)
    number_of_pages = models.IntegerField()
    cover_link = models.URLField()
    publication_language = models.CharField(max_length=255)
