from django.urls import path

from booksapp.views import BookListView, BookCreateView, BookUpdateView, BookDeleteView, book_import

urlpatterns = [
    path('', BookListView.as_view(), name='book_list'),
    path('book/add', BookCreateView.as_view(), name='book_create'),
    path('book/<int:pk>/update/', BookUpdateView.as_view(), name='book_update'),
    path('book/<int:pk>/delete/', BookDeleteView.as_view(), name='book_delete'),
    path('book/import/', book_import, name='book_import'),
]
